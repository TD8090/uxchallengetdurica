﻿; This script was created using Pulover's Macro Creator

#NoEnv
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Window
SendMode Input
#SingleInstance Force
SetTitleMatchMode 2
DetectHiddenWindows On
#WinActivateForce
SetControlDelay 1
SetWinDelay 0
SetKeyDelay -1
SetMouseDelay -1
SetBatchLines -1


^S::
IfWinActive, ahk_exe PhpStorm64.exe
{
	Sleep, 333
	WinActivate, ahk_exe IETester.exe
	Sleep, 333
	Send, {F5}
	Sleep, 555
	WinActivate, ahk_exe PhpStorm64.exe
	Sleep, 333
}
Else
{
Send {Ctrl}{S}
}
Return


F8::ExitApp
