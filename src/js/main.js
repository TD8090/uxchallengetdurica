/**
 *      HELPER SCRIPTS...
 *
 * */



    // Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});
    while (length--) {
        method = methods[length];
        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

/* getElementsByClassName Shiv - I found it to be 80-99% faster than other implementations in JSperf */
if (!document.getElementsByClassName) {
    document.getElementsByClassName = function(search) {
        var d = document, elements, pattern, i, results = [];
        if (d.querySelectorAll) { // IE8
            return d.querySelectorAll("." + search);
        }
        if (d.evaluate) { // IE6, IE7
            pattern = ".//*[contains(concat(' ', @class, ' '), ' " + search + " ')]";
            elements = d.evaluate(pattern, d, null, 0, null);
            while ((i = elements.iterateNext())) {
                results.push(i);
            }
        } else {
            elements = d.getElementsByTagName("*");
            pattern = new RegExp("(^|\\s)" + search + "(\\s|$)");
            for (i = 0; i < elements.length; i++) {
                if ( pattern.test(elements[i].className) ) {
                    results.push(elements[i]);
                }
            }
        }
        return results;
    }
}








seance = {
    /*??scan attrs to establish current nav/pg??
    * if str matches the current pg.id, exit(no action possible)
    * if not, take the str and establish goto elmts
    * Navs:: if utility pages were called(advsearch)
    *               -->run navOFF alone and skip over navON
    *               if gotoNav
    *       else run navOFF(currNav) and navON(gotoNav)
    *       ??and set currNav to our gotoNav for later??
    * PGs:: gotoPg display:block; seance(gotoPg, appears)
    *       seance(currPg, disappears)
    *       ??and now set currPg to our gotoPg for later??
    * */
    'init' : function(str){
        var x = this;
        x.gotoNav = document.getElementById("nav_"+str);
        x.gotoPg =  document.getElementById("pg_"+str);
        if(!x.gotoNav && x.currPg != x.gotoPg){/* !nav + diff page */
            x.navOFF(x.currNav);
            x.loadxhr(x.gotoPg, "pg_"+str);
            x.summon(x.gotoPg, 0, 100, 300);
            x.summon(x.currPg, 100, 0, 300);
            x.currPg = x.gotoPg;
        }
        else if(x.currPg != x.gotoPg) {
            x.navOFF(x.currNav);
            x.navON(x.gotoNav);
            x.loadxhr(x.gotoPg, "pg_"+str);
            x.summon(x.gotoPg, 0, 100, 300);
            x.summon(x.currPg, 100, 0, 300);
            x.currPg = x.gotoPg;
            x.currNav = x.gotoNav;
        }
    }
    ,'loadxhr' : function (dest, id) {var xhr;
        if (window.XMLHttpRequest) {xhr = new XMLHttpRequest();// IE7+, Firefox, Chrome, Opera, Safari
        } else {xhr = new ActiveXObject("Microsoft.XMLHTTP");}// IE6, IE5
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                dest.innerHTML = xhr.responseText;}};
        xhr.open("GET", id + ".html", true);
        xhr.send();
    }
    ,'navOFF' : function(n){
        n.style.backgroundColor = "#ECECEC";
        n.style.height = "24px";
        n.style.fontSize = "12px";
        n.style.fontWeight = "lighter";
    }
    ,'navON' : function(n){
        n.style.backgroundColor = "#FFFFFF";
        n.style.height = "34px";
        n.style.fontSize = "14px";
        n.style.fontWeight = "bold";
    }
    ,'summon' : function(element, start, STOP, duration) {// console.log("Conducting a seance...");
        console.log(element);
        var invocation,focus=Math.round(duration/100),psiLevel=0;
        if (start < STOP) {console.log('showing');
            for (u = start; u <= STOP; u++) {
                (function () {var orb = u;
                    setTimeout(function () {Ghostlier(element, orb, 1)
                    }, psiLevel * focus);}());psiLevel++;}
        } else {console.log('hiding');
            for (v = start; v >= STOP; v--) {
                (function () {var orb = v;
                    setTimeout(function () {Ghostlier(element, orb, 0)
                    }, psiLevel * focus);}());psiLevel++;}
        }
        function Ghostlier(ghost, ghostliness, realm) {
            invocation = 'opacity:' + ghostliness / 100
                + ';filter:alpha(opacity=' + ghostliness + ');';
            $(ghost).attr("style", invocation);
            if(realm==0 && ghost.style.opacity<=.02){ghost.style.visibility = "hidden";}
            else{ghost.style.visibility = "visible";}
        }
    }
    ,'popup': function(){
        var x = this;
    }
    ,posPopups: function(){
        var bcr;
        $('.popup').each(function(){
            bcr = this.getBoundingClientRect();
            var calcleft = (bcr.width)+"px";
            var calctop = (bcr.height)+"px";
            $('this').css({'top':calctop,'left':calcleft});
        });
    }
    ,'runOnce' : function(){
        var x = this;
        x.gotoNav = document.getElementById("nav_home");
        x.gotoPg =  document.getElementById("pg_home");
        x.navON(x.gotoNav);
        x.loadxhr(x.gotoPg, "pg_home");
        x.summon(x.gotoPg, 0, 100, 300);
        x.currNav = document.getElementById("nav_home");
        x.currPg = document.getElementById("pg_home");
        $('#c-pages-container').on('click','.acc-link',  function(evt) {
            $(evt.target).next('.content').siblings('.content').slideUp('fast');
            $(evt.target).next('.content').slideToggle('fast');
        }).on('click', '#catgen-btn',  function(e) {
            var newcat = new Image(), litterbox = document.getElementById("litterbox");
            newcat.onload = function(){litterbox.src = newcat.src;};
            newcat.src = 'http://lorempixel.com/300/400/cats?' + new Date().getTime();
        }).on('click','.popuplink',function (e){
            var targetup = $(this).next()[0];
            if (e.stopPropagation){e.stopPropagation();}else{e.cancelBubble=true;}
            if (e.preventDefault){e.preventDefault();}else{e.returnValue = false;}
            //x.posPopups();
            x.summon(targetup, 0, 100, 100);
            (function setOut(){
                $(document).one('click',function(e){
                    if (e.target == targetup.id) {
                        return setOut();
                    } else {x.summon(targetup, 100, 0, 100)}
                });
            }());
        })
    }
};
$(document).ready(seance.runOnce());

/** *
 * Here comes the hot stepper, I'm the lyrical gangster,
 *  Excuse me mister officer, Still love you like that!
 * */
